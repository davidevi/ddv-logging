import logging

from ddv.logging import log_to_stdout

logger = logging.getLogger(__name__)


class A(object):
    def __init__(self):
        self.logger = logging.getLogger(__name__ + ".A")
        self.logger.debug("Creating instance of A")
        self.b = B()
        self.A1()

    def A1(self):
        self.logger.info("A1 has been called")


class B(object):
    def __init__(self):
        self.logger = logging.getLogger(__name__ + ".B")
        self.logger.debug("Creating instance of B")
        self.c = C()
        self.B1()

    def B1(self):
        self.logger.info("B1 has been called")


class C(object):
    def __init__(self):
        self.logger = logging.getLogger(__name__ + ".C")
        self.logger.debug("Creating instance of C")
        self.C1()

    def C1(self):
        self.logger.info("C1 has been called")


def main():

    verbosity_filters = {1: ["__main__.A"], 2: ["__main__.B"], 3: ["__main__.C"]}

    log_to_stdout(
        logging_level=logging.DEBUG,
        enable_colours=True,
        enable_indentation=True,
        verbosity_filters=verbosity_filters,
        verbosity_level=2,
        message_format="%(levelname)s  %(message)s",
    )

    logger.info("Main has been called")
    A()
    logger.warning("Execution complete")


if __name__ == "__main__":
    main()
