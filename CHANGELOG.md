# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2021-08-24 
- Add ability to specify message format

## [0.2.2] - 2020-10-27
- Add CI pipeline for facilitating release process

## [0.2.1] - 2020-10-18
- Replace relative link to image in README.md with permalink

## [0.2.0] - 2020-10-18

### Added
- Add `IndentFormatter` for indenting records based on stack trace level
- Add `VerbosityFilter` for filtering out records coming from configurable modules
- Add usage example sample code


## [0.1.0] - 2020-04-13

### Added
- Created `ColouredFormatter` and `log_to_stdout` method
